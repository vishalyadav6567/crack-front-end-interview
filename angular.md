# what is Reactive programming.

Reactive programming is a programming paradigm that treats streams of data, called Observables, as its basic units of programming

# what is rxjs. how will u create observable. what all methods it has.
RxJS library is being used to bring `Reactive programming`(adaptable to change) to our application.
You remember previously `Object.Observe` was introduce in ECMA7 but later on becouse of browser's performance issue it got removed.

RxJS brings similar kind of functionality where we can create observable objects.
Methods like map, filter,reduce,take, takeWhile exists on simple streams and in my codebase most of the time I had used Observables or subjects.
Where we can subscribe them and in our subscription we get stream of data.

# what is diffrence between observable and promise

Obseravable can be canceled in middle but promises can not. Promises are given natively in javascript but we need libraries for observable e.g. rxJS

# what are changes done in angular 2,4,5,6.

When discussing about chenges within angular version 2,4,5,6 are very minimal syntexwize like in angular 2 syntax for using route/http were diffrent 
or there source library was diffrent but now they had changed.

Animation part is well improved in later version of angular.
Several bug fixes are done with performance improvement.

Most of the angular changes its version because of its dependency version changes.

# how you will communicate between two components.
  
  I can communicate among controller using following ways.
  
  1.  using window object(very bad way)
  2.  using @input, @output
  3.  using services and events(good way). 
  
  I personally like service way more because of its modular way and its simplicity.

# what is typescript, what all advantage it has.
Initially javascript was developped just to bring dynamicity to web pages but later on people started using it for bigger application.

Since javascript is a loosly typed language it is not compactable for bigger application. because if `Team A` is using module developped by 
`Team B` it will be hard to know what their methods signature is. Typescript solves this issue for us by introducing type.

*Pros:* 

1. Working on bigger/complex application becomes easy.
2. performance of our application increases since memory consumption by type conversion get reduced at some extent.

*Cons:*

1. it is not supported by browsers/javascript engines.
2. it includes some learning curve.

# what is zoneJS, what all advantage it has.
A zone is an execution context that persists across async tasks, and allows the creator of 
the zone to observe and control execution of the code within the zone.
*Pros: *
1. Resource utilisation goes high
# what is AOT
# what is providers in angular
# how you will protect route
# how SEO can be done
# what is directive. create a custom directive
# what is filter in angularJS. create a custom filter
# what all lifecycle it has
# How you changed your application from angularJS to angular
