# what is closure
ES5 has three kind of scope 1st global 2nd functional leval 3rd is closure.
it says that variable created inside outer function can be used inside inner function.

Benefits: 

1. we can create private variable by using it.
2. we can create closure for async methods if they are in loop.

Cons:
1. since scope of variable keeps alive they are not getting garbase collected so more memory uses will be there.

# what variable and function hoisting
variable defined or created any where in scope there definition goes on top is termed as variable hoisting.
function defined or created any where in scope there definition and implementation too goes on top is termed as function hoisting.

# what is difference between var, let, const
var : used for variable creation and has only three scope global,functional, closure.

let : used for variable creation but its scope is valid inside block only variable created by using it can be registered for 
GC(garbage collection) after coming out of the block.

const : used for variable creation and is similar as final in java once initialised can not change its value.

# what is difference between call, apply and bind
these all method are being used for attaching the context(value of `this`) but bind does not expect any argument where call expect 
object of argument and apply expect array of argument.

When I'm not having any argument I'll use bind. when I'm not certain for the length of argument I'll use apply else if I know all argument 
I'll use call.

# what is promise

A promise is used to track a task which will execute in future and can go in two phases e.g. resolved/rejected

# what all http method are there and their significance
Get: It is being used to fetch the data.
Post: We use it when we want to send some data to server and want something in return.
Put: It is being just to put some data on server.
delete: It is being used to delete some data on server.
Option: For other purpose we use it like validating tokens etc.

Note: These are just for standards. We are not strictly bound to use them for given purpose.
# what is the output of following code and how you will rectify it
```
for(var i = 0; i<100;i++){
    setTimeout(function(){
        console.log(i);
    },1000);
} 
```

It will print 100 hundred times because loop will get executed initially and callback passed to setTimeout method will be pushed to the end of 
event loop. so they will executed when `i` will be already set to its limit i.e. 100

We can reactify it in following ways:

- Using closure
```for(var i = 0; i<100;i++){
   (function(i){
       setTimeout(function(){
              console.log(i);
       },1000);
   })(i)
} 
```
- Using let keyword
```
for(let i = 0; i<100;i++){
    setTimeout(function(){
        console.log(i);
    },1000);
} 

```
# what is major difference between ECMA5 and ECMA6
See link (http://es6-features.org/#Constants)

Classes, Arrow Functions, Block Scope using let keyword, Promises etc


# what is ECMA
ECMA is an organisation who standardise javascript.

# Why javascript is your favorite language
It is my favorite language because of its simplicity and diversity to use. If I know javascript I can work on any part of application 
Front-End(react, angular etc.), TDD testing(kerma, mocha, chai), BDD testing(protractor), backend or micro-services using nodeJS, 
DataBase in MongoDB, IOT or embeded system using jerryscript etc.  

# what is callback hell and how you will remove it

long or iterative nesting of asynchronous functions is known as callback hell.

We can remove it in two ways:
 1. By using promises
 2. By using async await
